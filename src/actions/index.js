import http from "../api";
import config from "../config.json";

let nextProductId = 0;

export const clearResult = () => {
    return {
        type: 'CLEAR_RESULT',
    }
};

export const clearErrors = () => {
    return {
        type: 'CLEAR_ERRORS',
    }
};


const addProductSuccess = result => ({
    type: 'ADD_PRODUCT_SUCCESS',
    payload: result,
    id: nextProductId++
});

const addProductFail = result => ({
    type: 'ADD_PRODUCT_FAIL',
    payload: result,
});

export const addProduct = data => dispatch =>
    http(config.url + 'product')
        .post(data)
        .then(result => {
            dispatch(addProductSuccess(result));
        })
        .catch(err => {
            dispatch(addProductFail(err));
        });


const deleteProductSuccess = productId => ({
    type: 'DELETE_PRODUCT_SUCCESS',
    payload: productId,
    id: nextProductId--
});

const deleteProductFail = result => ({
    type: 'DELETE_PRODUCT_FAIL',
    payload: result,
});

export const deleteProduct = productId => dispatch =>
    http(config.url + 'product/' + productId)
        .delete()
        .then(result => {
            dispatch(deleteProductSuccess({id: productId, result: result}));
        })
        .catch(err => {
            dispatch(deleteProductFail(err));
        });


const fetchProductsSuccess = products => ({
    type: 'FETCH_PRODUCTS_SUCCESS',
    payload: products
});

const fetchProductsFail = products => ({
    type: 'FETCH_PRODUCTS_FAIL',
    payload: products
});

export const fetchProducts = () => dispatch =>
    http(config.url + 'product')
        .get()
        .then(products => dispatch(fetchProductsSuccess(products)))
        .catch(err => {
            dispatch(fetchProductsFail(err));
        });