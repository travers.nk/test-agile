import React, {Component} from 'react';
import NavBar from './NavBar';

class Layout extends Component {

    render() {
        const Inner = this.props.inner;
        return (
           <div>
               <NavBar/>
               <Inner/>
           </div>
        )
    }
}

export default Layout;