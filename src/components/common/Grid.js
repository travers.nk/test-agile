import React, { Component } from 'react';
import ReactTable from "react-table";
import "react-table/react-table.css";
import { Button } from 'react-bootstrap';



class Grid extends Component {

    constructor (props) {
        super(props);
        this.state = {
            data: this.props.products,
        };
        this.actionRelease = this.actionRelease.bind(this);
    };

    componentWillReceiveProps(nextProps) {
        if (this.props !== nextProps){
            this.setState({
                data: nextProps.products,
                }
            );
        }
    }

    actionRelease(e){
        const id = e.target.id;
        this.props.action(id);
    }

    render() {
        const {data}  = this.state;
        console.log(data);
        return (
            <div>
                <ReactTable
                    data={data}
                    columns={[{
                        Header: 'Product ID',
                        accessor: 'id'
                    },{
                        Header: 'Product name',
                        accessor: 'name'
                    }, {
                        Header: 'Colors',
                        accessor: 'Colors',
                        Cell: row => (
                            console.log(row),
                           row.row.Colors.map(color => { return (
                            <span  key={color.id}>
                                <span style={{
                                    color : color.name.trim() === 'red' ? '#ff2e00'
                                          : color.name.trim() === 'blue' ? '#2735ff'
                                          : '#57d500',
                                    transition: 'all .3s ease'
                                }}> &#x25cf;
                                </span>
                           </span>
                        )}
                        ))},{
                        Header: 'Actions',
                        accessor: 'id',
                        Cell: row => (
                                <Button
                                    id = {row.row.id}
                                    bsStyle="warning"
                                    type="submit"
                                    onClick={this.actionRelease}
                                >
                                    {this.props.action_name}
                                </Button>
                        )
                    }
                    ]}
                    defaultPageSize={10}
                    className="-striped -highlight"
                />
                <br />
            </div>
        );
    }

}
export default Grid;