import React, { Component } from 'react';
import { FormGroup, FormControl, Button, HelpBlock } from 'react-bootstrap';
import CheckBoxGroup from "./controls/CheckBoxGroup";
import Input from "./controls/Input";


class Form extends Component {

    constructor (props) {
        super(props);
        this.state = {
            name: '',
            colors: [],
        };
        this.getValidationState = this.getValidationState.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleColorSelection = this.handleColorSelection.bind(this);
        this.handleClearForm = this.handleClearForm.bind(this);
    };

    getValidationState() {
       const rex = /^[A-Za-z0-9]{4,8}$/;
        const rexMatch = this.state.name.match(rex);
        if (rexMatch) {
            return 'success';
        }
        else if (!rexMatch) {
            return 'error';
        }
        return null;
    };
    clearMessages() {
        this.props.actionClearResult();
        this.props.actionClearErrors();
    }

    handleInputChange(e) {
        e.preventDefault();
        this.clearMessages();
        this.setState({
            name: e.target.value
        });
    };

    handleColorSelection(e) {
        this.clearMessages();
        const newSelection = e.target.value;
        let newSelectionArray;

        if(this.state.colors.indexOf(newSelection) > -1) {
            newSelectionArray = this.state.colors.filter(s => s !== newSelection)
        } else {
            newSelectionArray = [...this.state.colors, newSelection];
        }

        this.setState({
            colors: newSelectionArray
        });
    }

    handleSubmit(e){
        this.clearMessages();
        e.preventDefault();
        this.props.action({'name': this.state.name, "colors": this.state.colors.join(",")});
        this.handleClearForm(e);
    }

    handleClearForm(e) {
        e.preventDefault();
        this.setState({
            name: '',
            colors: []
        });
    }

    render() {

        return (
            <form>
                <FormGroup controlId="name"
                           validationState={this.getValidationState()}>
                    <Input
                        inputType={'text'}
                        title={'Product name'}
                        name={'product'}
                        controlFunc={this.handleInputChange}
                        content={this.state.name}
                        placeholder={'Type product name here'} />
                    <FormControl.Feedback />
                    <HelpBlock>
                        Product name should be alphanumeric and its length should be from 4 to 8
                    </HelpBlock>
                </FormGroup>

                <br/>

                <CheckBoxGroup
                    title={'Which color?'}
                    setName={'colors'}
                    type={'checkbox'}
                    controlFunc={this.handleColorSelection}
                    options={['red', 'blue', 'green']}
                    selectedOptions={this.state.colors} />

                <Button bsStyle="success" type="submit" onClick={this.handleSubmit}>
                    Add {this.props.name}
                </Button>

                <Button bsStyle="warning" type="reset" onClick={this.handleClearForm}>
                    Clear
                </Button>
            </form>

        )
    }
}
export default Form;
