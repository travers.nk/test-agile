import React from "react";

class NotFound extends React.Component {
    render() {
        return (
            <div>
                <h2>404</h2>
                <p>Not Found</p>
            </div>
        );
    }
}

export default NotFound
