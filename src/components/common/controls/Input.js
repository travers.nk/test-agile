import React from 'react';
import PropTypes from "prop-types";
import { FormGroup, FormControl, Button, HelpBlock, ControlLabel, Checkbox } from 'react-bootstrap';

const Input = (props) => (
    <FormGroup>
        <ControlLabel>{props.title}</ControlLabel>
        <FormControl
            className="form-input"
            name={props.name}
            type={props.inputType}
            value={props.content}
            onChange={props.controlFunc}
            placeholder={props.placeholder}
        />
    </FormGroup>
);

Input.propTypes = {
    inputType: PropTypes.oneOf(['text', 'number']).isRequired,
    title: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    controlFunc: PropTypes.func.isRequired,
    content: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]).isRequired,
    placeholder: PropTypes.string,
};

export default Input;