import React from 'react';
import PropTypes from "prop-types";
import { ControlLabel } from 'react-bootstrap';


const CheckBoxGroup = (props) => (
    <div>
        <ControlLabel>{props.title}</ControlLabel>
        <div className="checkbox-group">
            {props.options.map(opt => {
                return (
                    <ControlLabel key={opt} className="form-label capitalize">
                        <input
                            className="form-checkbox"
                            name={props.setName}
                            onChange={props.controlFunc}
                            value={opt}
                            checked={ props.selectedOptions.indexOf(opt) > -1 }
                            type={props.type}
                        />  {opt}
                    </ControlLabel>
                );
            })}
        </div>
    </div>
);

CheckBoxGroup.propTypes = {
    title: PropTypes.string.isRequired,
    type: PropTypes.oneOf(['checkbox', 'radio']).isRequired,
    setName: PropTypes.string.isRequired,
    options: PropTypes.array.isRequired,
    selectedOptions: PropTypes.array,
    controlFunc: PropTypes.func.isRequired
};

export default CheckBoxGroup;