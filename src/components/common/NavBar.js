import React, {Component} from 'react';
import { Navbar, Nav, NavItem } from 'react-bootstrap';

class NavBar extends Component {
    constructor(props) {
        super(props);
    }


    render(){
        return (
            <Navbar>
                <Navbar.Header>
                    <Navbar.Brand>
                        <span>Agile Engine</span>
                    </Navbar.Brand>
                </Navbar.Header>
                <Nav>
                    <NavItem eventKey={1} href="/">Products List Page</NavItem>
                    <NavItem eventKey={2} href="/add-product">Add Product</NavItem>
                </Nav>
            </Navbar>
        )
    }
}

export default NavBar;