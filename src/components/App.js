import React from 'react';
import {Route, Switch} from "react-router";
import NotFound from "./common/NotFound";
import ListProductsPage from "../pages/ListProductsPage";
import AddProductPage from "../pages/AddProductPage";

const App = () => (
    <Switch >
        <Route exact path='/' component={ListProductsPage}/>
        <Route path='/add-product' component={AddProductPage}/>
        <Route path="*" component={NotFound}/>
    </Switch>
);

export default App
