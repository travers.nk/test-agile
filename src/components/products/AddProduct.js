import React, { Component } from 'react'
import { connect } from 'react-redux'
import { addProduct, clearResult, clearErrors } from '../../actions'
import InfoPanel from "../common/InfoPanel";
import Form from "../common/Form";


class AddProduct extends Component {

    constructor (props) {
        super(props);
        this.state = {
            success: ''
        };
    };

    componentWillReceiveProps(nextProps) {
        if (this.props !== nextProps){
            this.setState({
                    success: nextProps.result
                }
            );
        }
    }

    render(){
        return (
            <div>
                <InfoPanel success={this.state.success} errors={this.props.errors} />
                <Form
                    action={this.props.addProduct}
                    actionClearResult={this.props.clearResult}
                    actionClearErrors={this.props.clearErrors}
                    name="product"/>
            </div>
        )
    }
}

const mapStateToProps = store => ({
    result: store.result,
    errors: store.errors,
});

const mapDispatchToProps = dispatch => ({
    addProduct: (product) => dispatch(addProduct(product)),
    clearResult: () => dispatch(clearResult()),
    clearErrors: () => dispatch(clearErrors())
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AddProduct);