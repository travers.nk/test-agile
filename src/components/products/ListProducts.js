import React, {Component} from 'react';
import { connect } from 'react-redux';
import { fetchProducts, deleteProduct } from '../../actions'
import Grid from "../common/Grid";
import InfoPanel from "../common/InfoPanel";
import {clearResult, clearErrors} from "../../actions/index";


class ListProducts extends Component {

    constructor (props) {
        super(props);
        this.state = {
            success: ''
        };
    };

    componentDidMount() {
        this.props.clearResult();
        this.props.clearErrors();
        this.props.fetchProducts();
    }

    componentWillReceiveProps(nextProps) {
        if (this.props !== nextProps){
            this.setState({
                    success: nextProps.result
                }
            );
        }
    }

    render() {
        return (
            <div>
                <InfoPanel success={this.state.success} errors={this.props.errors} />
                <Grid
                    products={this.props.products}
                    action={this.props.deleteProduct}
                    action_name={'delete'}
                />
            </div>
        );
    }
}

const mapStateToProps = store => ({
    products: store.products,
    result: store.result,
    errors: store.errors,
});

const mapDispatchToProps = dispatch => ({
    fetchProducts: () => dispatch(fetchProducts()),
    deleteProduct: (productId) => dispatch(deleteProduct(productId)),
    clearResult: () => dispatch(clearResult()),
    clearErrors: () => dispatch(clearErrors())
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ListProducts);