import React, {Component} from 'react';
import AddProduct from "../components/products/AddProduct";
import Layout from "../components/common/Layout";


class AddProductPage extends Component {

    render() {
        return (
            <div>
                <Layout inner={AddProduct} {...this.props}/>
            </div>
        );
    }
}

export default AddProductPage;