import React, {Component} from 'react';
import ListProducts from "../components/products/ListProducts";
import Layout from "../components/common/Layout";


class ListProductsPage extends Component {

    render() {
        return (
            <div>
                <Layout inner={ListProducts} {...this.props}/>
            </div>
        );
    }
}

export default ListProductsPage;