import {combineReducers} from 'redux';
import result from "./result";
import products from "./products";
import errors from "./errors";


const app = combineReducers({
    result,
    products,
    errors,
});

export default app
