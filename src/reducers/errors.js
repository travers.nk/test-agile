const errors = (store = [], action) => {
    switch (action.type) {
        case 'ADD_PRODUCT_FAIL':
            return JSON.parse(action.payload.errors);
        case 'FETCH_PRODUCTS_FAIL':
            return JSON.parse(action.payload.errors);
        case 'DELETE_PRODUCT_FAIL':
            return JSON.parse(action.payload.errors);
        case 'CLEAR_ERRORS':
            return [];
        default:
            return store;
    }
};

export default errors;