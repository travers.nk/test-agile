const products = (store = [], action) => {

    switch (action.type) {

        case 'ADD_PRODUCT_SUCCESS':
            return [
                ...store,
                {
                    id: action.id,
                    name: action.payload.data.name,
                    colors: action.payload.data.colors,
                }
            ];
        case 'DELETE_PRODUCT_SUCCESS':
            const newStore = Object.assign([], store);
            const indexOfProductToDelete = store.findIndex(product => {
                return Number(product.id) ===  Number(action.payload.id);
            });
            newStore.splice(indexOfProductToDelete, 1);
            return newStore;
        case 'FETCH_PRODUCTS_SUCCESS':
            return action.payload;
        case 'CLEAR':
            return [];
        default:
            return store;
    }
};

export default products;