const result = (store = [], action) => {
    switch (action.type) {
        case 'ADD_PRODUCT_SUCCESS':
            return action.payload.message;
        case 'DELETE_PRODUCT_SUCCESS':
            return action.payload.result.message;
        case 'CLEAR_RESULT':
            return '';
        case 'ADD_PRODUCT_FAIL':
            return action.payload.status;
        default:
            return store;
    }
};

export default result;