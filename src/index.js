import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter} from 'react-router-dom';
import {applyMiddleware, createStore} from 'redux';
import {Provider} from 'react-redux';
import {createLogger} from 'redux-logger'
import thunk from 'redux-thunk';
import {compose} from "recompose";
import app from './reducers';
import './index.css';
import App from './components/App';
import registerServiceWorker from './registerServiceWorker';


let store = createStore(app,
    compose(
        applyMiddleware(createLogger()),
        applyMiddleware(thunk)
    )
);

ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            <App/>
        </BrowserRouter>
    </Provider>
    , document.getElementById('root'));
registerServiceWorker();